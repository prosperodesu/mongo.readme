REQUIREMENTS
------------

The minimum requirement by this application template that your Web server supports PHP 5.4.0.

MongoDB
------------
### install on server(example on ubuntu)
**Required opened inbound TCP ports: 27017, 28017**


-Execute commands for install:
```
1. sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
2. echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list
3. sudo apt-get update
4. sudo apt-get install -y mongodb-org
Then start mongodb
5. sudo service mongod start
-Create user and database
1. start mongod:
mongod --port 27017 --dbpath /path_to_database/
2. connect to mongo shell:
mongo --port 27017
3. create user in shell:


# create root user
use admin
db.createUser(
  {
    user: "admin",
    pwd: "admin",
    roles: [ { role: "root", db: "admin" } ]
  }
);


use mydatabase
db.createUser(
  {
    user: "myuser",
    pwd: "12345",
    roles: [ { role: "dbOwner", db: "mydatabase" } ]
  }
)
```

Setup user:
https://www.howtoforge.com/tutorial/install-mongodb-on-ubuntu-16.04/

4. exit from shell
5. test auth mongo --port 27017 -u "myuser" -p "SECRETPASSWORD" --authenticationDatabase "mydatabase"

Auth setup:
https://docs.mongodb.com/manual/tutorial/enable-authentication/

If lost access:
1. service mongod stop
2. start daemon without auth 
mongod --dbpath /var/lib/mongodb
3. create or update users 

For Debian: https://docs.mongodb.com/v3.2/tutorial/install-mongodb-on-debian/

### install php mongo extension
1. install php-pear:
sudo apt-get install php-pear
2. sudo pecl install mongodb
3. add to php.ini
extension=mongodb.so
4. restart server

### example usage for mongodb yii2 extension

```php
use Yii;
use yii\mongodb\Query as mongoQuery;
$collection = Yii::$app->mongodb->getCollection('test');
$collection->insert(['name' => 'John Smith', 'status' => 1]);
# Query
$query = new mongoQuery();
$query->select(['name', 'status'])
    ->from('test')
    ->limit(10);
$rows = $query->all();
```

Mongo Shell Commands
https://docs.mongodb.com/v3.2/reference/method/